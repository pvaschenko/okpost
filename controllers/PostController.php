<?php
namespace app\controllers;
use Yii;
use app\models\Post;
use yii\web\Controller;
use yii\web\UploadedFile;

class PostController extends Controller
{
    public function actionCreate()
    {
        $model = new Post();
        if( $model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                if ($model->upload()) {
                    Yii::$app->session->setFlash('success', 'Изображение загружено');
                    $model->file = null;
                }
            }

            //Сюда необходимо вставить ключи
            $result = Yii::$app->okpostcomponent->postOk('','','','',$model->message,$model->picture);

            if ($result === true)  {
                if( $model->save() ) {
                    return $this->refresh();
                }
            } else {
                Yii::$app->session->setFlash('error', "Ошибка API OK.ru $result");
            }
        }
        return $this->render('create', ['model' => $model]);
    }
}