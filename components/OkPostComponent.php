<?php

namespace app\components;

use yii\base\Component;

class OkPostComponent extends Component
{
    const OKAPI = 'https://api.ok.ru/fb.do';
    const LIMIT = 5;

    /** Функция запроса к API Ok.ru
     * @param string $url
     * @param string $type
     * @param array $params
     * @param int $timeout
     * @param bool $image
     * @param bool $decode
     * @param int $count
     * @return void
     */
    private function getUrl($url, $type = "GET", $params = array(), $timeout = 30, $image = false, $decode = true, $count = 0)
    {
        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);

            if ($type == "POST") {
                curl_setopt($ch, CURLOPT_POST, true);

                // Картинка
                if ($image) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                } // Обычный запрос
                elseif ($decode) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
                } // Текст
                else {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
                }
            }

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot');
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            $data = curl_exec($ch);

            curl_close($ch);

            // Еще разок, если API завис
            if (isset($data['error_code']) && $data['error_code'] == 5000 && $count <= self::LIMIT) {
                $data = $this->getUrl($url, $type, $params, $timeout, $image, $decode, $count++);
            }
            return $data;
        } else {
            return "{}";
        }
    }

   /** Функция публикации
   * @param string $okAccessToken
   * @param string $okPrivateKey
   * @param string $okPublicKey
   * @param string $okGroupId
   * @param string $message
   * @param string $picture
   * @return bool True if the post is placed, else code of error.
   */
    public function postOk($okAccessToken,$okPrivateKey,$okPublicKey,$okGroupId,$message,$picture)
    {
        // 1. Получим адрес для загрузки 1 фото

        $params = array(
            "application_key" => $okPublicKey,
            "method" => "photosV2.getUploadUrl",
            "count" => 1,  // количество фото для загрузки
            "gid" => $okGroupId,
            "format" => "json"
        );

        // Подпишем запрос
        $sig = md5($this->arInStr($params).md5("{$okAccessToken} {$okPrivateKey}"));

        $params['access_token'] = $okAccessToken;
        $params['sig'] = $sig;

        // Выполним
        $step1 = json_decode($this->getUrl(self::OKAPI, "POST", $params), true);

        // Если ошибка
        if (isset($step1['error_code'])) {
            // Обработка ошибки
            return $step1['error_code'];
        }

        // Идентификатор для загрузки фото
        $photoId = $step1['photo_ids'][0];

        // 2. Закачаем фотку

        // Предполагается, что картинка располагается в каталоге со скриптом
        $params = array(
            "pic1" => $picture,
        );

        // Отправляем картинку на сервер, подписывать не нужно
        $step2 = json_decode($this->getUrl($step1['upload_url'], "POST", $params, 30, true), true);

        // Если ошибка
        if (isset($step2['error_code'])) {
            // Обработка ошибки
            return $step2['error_code'];
        }

        // Токен загруженной фотки
        $token = $step2['photos'][$photoId]['token'];

        // Заменим переносы строк, чтоб не вываливалась ошибка аттача
        $messageJson = str_replace("\n", "\\n", $message);

        // 3. Запостим в группу
        $attachment['media'] =
        [
          [
              'type' => 'text',
              'text' => $messageJson
          ],
          [
              'type' => 'photo',
              'list' =>
                 [
                     ['id' => $token]
                 ]
          ]
        ];

        $params = array(
            "application_key" => $okPublicKey,
            "method" => "mediatopic.post",
            "gid" => $okGroupId,
            "type" => "GROUP_THEME",
            "attachment" => json_encode($attachment),
            "format" => "json",
        );

        // Подпишем
        $sig = md5($this->arInStr($params) . md5("{$okAccessToken}{$okPrivateKey}"));

        $params['access_token'] = $okAccessToken;
        $params['sig'] = $sig;

        $step3 = json_decode($this->getUrl(self::OKAPI, "POST", $params, 30, false, false), true);

        // Если ошибка
        if (isset($step3['error_code'])) {
            // Обработка ошибки
            return $step3['error_code'];
        }

        // Успешно
        return true;
    }

    /** Массив аргументов в строку
    * @param array $array
    * @return string
    */
    private function arInStr($array)
    {
        ksort($array);
        $string = "";
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $string .= $key . "=" . $this->arInStr($val);
            } else {
                $string .= $key . "=" . $val;
            }
        }
        return $string;
    }
}