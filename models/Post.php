<?php
namespace app\models;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * Class Post
 *
 * @property string message Сообщение.
 * @property string picture Картинка.
 * @property integer ok_post_id id поста в одноклассниках.
 * @property integer updated_at Когда пост был обновлен.
 * @property integer created_at Когда пост была создан.
 *
 * @package app\models
 */
class Post extends ActiveRecord
{
    public $file;

    public static function tableName()
    {
        return 'posts';
    }

    public function rules()
    {
        return [
            [['message'], 'required'],
            [['message'], 'trim'],
            ['message', 'string', 'max' => 500],
            ['file', 'image',
                'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
                'checkExtensionByMimeType' => true,
            ],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $dir = 'uploads/'; // Директория - должна быть создана
            $name = $this->randomFileName($this->file->extension);
            $file = $dir . $name;
            $this->picture = $file;
            $this->file->saveAs($file); // Сохраняем файл
            return true;
        } else {
            return false;
        }
    }

    private function randomFileName($extension = false)
    {
        $extension = $extension ? '.' . $extension : '';
        $name = md5(microtime() . rand(0, 1000));
        $file = $name . $extension;
        return $file;
    }
}